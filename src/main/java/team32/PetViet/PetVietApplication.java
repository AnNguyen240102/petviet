package team32.PetViet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetVietApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetVietApplication.class, args);
	}

}
