package team32.PetViet.controller;

import team32.PetViet.entity.Shiper;  
import team32.PetViet.service.ShiperService;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Controller;  
import org.springframework.ui.Model;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;  
import java.util.Optional;  

@Controller
public class ShiperController {
	@Autowired 
	private ShiperService shiperService;  
	
	@RequestMapping("/homeShiper")  
	  public String index(Model model) {  
	    List<Shiper> shipers = shiperService.getAllShiper();  

	    model.addAttribute("shipers", shipers);  

	    return "admin/shiper/HomeShiper";  
	  }  

	  @RequestMapping(value = "add")  
	  public String addShiper(Model model) {  
	    model.addAttribute("shiper", new Shiper());  
	    return "admin/shiper/AddUpdateShiper";  
	  }  

	  @RequestMapping(value = "/edit", method = RequestMethod.GET)  
	  public String editShiper(@RequestParam("id") Integer shiperId, Model model) {  
	    Optional<Shiper> shiperEdit = shiperService.findShiperById(shiperId);  
	    shiperEdit.ifPresent(shiper -> model.addAttribute("shiper", shiper));  
	    return "admin/shiper/AddUpdateShiper";  
	  }  

	  @RequestMapping(value = "save", method = RequestMethod.POST)  
	  public String save(Shiper shiper) {  
	    shiperService.saveShiper(shiper);  
	    return "redirect:/";  
	  }  

	  @RequestMapping(value = "/delete", method = RequestMethod.GET)  
	  public String deleteShiper(@RequestParam("id") Integer shiperId, Model model) {  
	    shiperService.deleteShiper(shiperId);  
	    return "redirect:/";  
	  }  
	  
	  @RequestMapping("/listShiperOrder")  
	  public String allShiperOrderNow(Model model) {  
	    List<Shiper> shipers = shiperService.allShiperOrderNow();  
	    model.addAttribute("shipers", shipers);
	    System.out.println(shipers.size());
	    return "admin/shiper/ListShiper";  
	  }  
	  
	  
}
