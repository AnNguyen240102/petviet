package team32.PetViet.entity;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;  

@Entity  
@Table(name = "tbl_shipers_transport")
public class Shiper {
	 @Id  
	 @GeneratedValue(strategy = GenerationType.IDENTITY)  
	  private Integer id;  

	  @Column(name = "name",length = 200, nullable = false )  
	  private String name;  

	  @Column(name = "price", nullable = false)  
	  private BigDecimal price;  
	  
	  
	  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
	  "shiper") private Set<Order> orders = new HashSet<Order>();
		 

	  
	  public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public BigDecimal getPrice() {
			return price;
		}

		public void setPrice(BigDecimal price) {
			this.price = price;
		}

		
		
	 
}
