/*
 * package team32.PetViet.entity;
 * 
 * import java.math.BigDecimal; import java.util.Date;
 * 
 * import javax.persistence.Column; import javax.persistence.Entity; import
 * javax.persistence.FetchType; import javax.persistence.GeneratedValue; import
 * javax.persistence.GenerationType; import javax.persistence.Id; import
 * javax.persistence.JoinColumn; import javax.persistence.ManyToOne; import
 * javax.persistence.Table;
 * 
 * @Entity
 * 
 * @Table(name = "tbl_products") public class Product {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Integer id;
 * 
 * @Column(name = "name", length = 200, nullable = false) private String name;
 * 
 * @ManyToOne(fetch = FetchType.EAGER)
 * 
 * @JoinColumn(name = "category_id") // foreign key private Categories
 * categories;
 * 
 * 
 * kieu long text
 * 
 * // @Lob
 * 
 * @Column(name = "description",columnDefinition = "LONGTEXT", nullable = false)
 * private String description;
 * 
 * @Column(name = "created_at", nullable = true) private Date createdDate;
 * 
 * @Column(name = "updated_at", nullable = true) private Date updatedDate;
 * 
 * BigDecimal
 * 
 * @Column(name = "price", precision = 13, scale = 2, nullable = false) private
 * BigDecimal price;
 * 
 * @Column(name = "dateofmanufacture", nullable = true) private Date
 * ngaySanXuat;
 * 
 * @Column(name = "expiry", nullable = true) private Date hanSuDung;
 * 
 * @Column(name = "status" , nullable = true) private Integer status;
 * 
 * @Column(name = "avg_rating", precision = 13, scale = 2, nullable = true)
 * private BigDecimal avgRating;
 * 
 * 
 * public Product() { super(); // TODO Auto-generated constructor stub }
 * 
 * public Product(Integer id, String name, String description, Date createdDate,
 * Date updatedDate, BigDecimal price, Date ngaySanXuat, Date hanSuDung, Integer
 * status, BigDecimal avgRating, Categories categories) { super(); this.id = id;
 * this.name = name; this.description = description; this.createdDate =
 * createdDate; this.updatedDate = updatedDate; this.price = price;
 * this.ngaySanXuat = ngaySanXuat; this.hanSuDung = hanSuDung; this.status =
 * status; this.avgRating = avgRating; this.categories = categories; }
 * 
 * public Integer getId() { return id; }
 * 
 * public void setId(Integer id) { this.id = id; }
 * 
 * public String getName() { return name; }
 * 
 * public void setName(String name) { this.name = name; }
 * 
 * public String getDescription() { return description; }
 * 
 * public void setDescription(String description) { this.description =
 * description; }
 * 
 * public Date getCreatedDate() { return createdDate; }
 * 
 * public void setCreatedDate(Date createdDate) { this.createdDate =
 * createdDate; }
 * 
 * public Date getUpdatedDate() { return updatedDate; }
 * 
 * public void setUpdatedDate(Date updatedDate) { this.updatedDate =
 * updatedDate; }
 * 
 * public BigDecimal getPrice() { return price; }
 * 
 * public void setPrice(BigDecimal price) { this.price = price; }
 * 
 * public Date getNgaySanXuat() { return ngaySanXuat; }
 * 
 * public void setNgaySanXuat(Date ngaySanXuat) { this.ngaySanXuat =
 * ngaySanXuat; }
 * 
 * public Date getHanSuDung() { return hanSuDung; }
 * 
 * public void setHanSuDung(Date hanSuDung) { this.hanSuDung = hanSuDung; }
 * 
 * public Integer getStatus() { return status; }
 * 
 * public void setStatus(Integer status) { this.status = status; }
 * 
 * public BigDecimal getAvgRating() { return avgRating; }
 * 
 * public void setAvgRating(BigDecimal avgRating) { this.avgRating = avgRating;
 * }
 * 
 * public Categories getCategories() { return categories; }
 * 
 * public void setCategories(Categories categories) { this.categories =
 * categories; }
 * 
 * 
 * 
 * }
 */