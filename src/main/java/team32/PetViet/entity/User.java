/*
 * package team32.PetViet.entity;
 * 
 * import java.util.Date;
 * 
 * import javax.persistence.Column; import javax.persistence.Entity; import
 * javax.persistence.GeneratedValue; import javax.persistence.GenerationType;
 * import javax.persistence.Id; import javax.persistence.Table;
 * 
 * @Entity
 * 
 * @Table(name = "tbl_users") public class User {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Long id;
 * 
 * @Column(name = "name", nullable = true) private String name;
 * 
 * @Column(name = "username", nullable = true) private String username;
 * 
 * @Column(name = "password", nullable = true) private String password;
 * 
 * @Column(name = "email", nullable = true) private String email;
 * 
 * @Column(name = "adress", nullable = true) private String adress;
 * 
 * @Column(name = "cmt", nullable = true) private String cmt;
 * 
 * @Column(name = "created_at", nullable = true) private Date createdAt;
 * 
 * @Column(name = "updated_at", nullable = true) private Date updateAt;
 * 
 * @Column(name = "phone", nullable = true) private String phone;
 * 
 * @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
 * "user") private Set<ProductsEnity> productsEnities = new
 * HashSet<ProductsEnity>(); }
 */