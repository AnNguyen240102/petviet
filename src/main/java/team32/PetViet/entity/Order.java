package team32.PetViet.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "tbl_order")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/*
	 * @ManyToOne(fetch = FetchType.EAGER)
	 * 
	 * @JoinColumn(name = "user_id")//foreign key private User user;
	 */
	
	@Column(name = "total_price", nullable = true)
	private BigDecimal totalPrice;
	
	@Column(name = "created_at", nullable = false)
	private Date createdAt;
	
	
	@Column(name = "updated_at", nullable = false)
	private Date updateAt;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "shipping_id")//foreign key
	private Shiper shiper;
	
	@Column(name = "status", nullable = false)
	private Integer status;
	
	/*
	 * @ManyToOne(fetch = FetchType.EAGER)
	 * 
	 * @JoinColumn(name = "contact_id")//foreign key private Contact contact;
	 */
	
	
	/*
	 * @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
	 * "Order") private Set<OrderDetails> orderDetails = new
	 * HashSet<OrderDetails>();
	 */
	
	
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/*
	 * public User getUser() { return user; }
	 */

	/*
	 * public void setUser(User user) { this.user = user; }
	 */

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public Shiper getShiper() {
		return shiper;
	}

	public void setShiper(Shiper shiper) {
		this.shiper = shiper;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

//	public Contact getContact() {
//		return contact;
//	}
//
//	public void setContact(Contact contact) {
//		this.contact = contact;
//	}

	

}
