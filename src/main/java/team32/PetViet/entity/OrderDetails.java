/*
 * package team32.PetViet.entity;
 * 
 * import javax.persistence.Column; import javax.persistence.Entity; import
 * javax.persistence.FetchType; import javax.persistence.GeneratedValue; import
 * javax.persistence.GenerationType; import javax.persistence.Id; import
 * javax.persistence.JoinColumn; import javax.persistence.ManyToOne; import
 * javax.persistence.Table;
 * 
 * @Entity
 * 
 * @Table(name = "tlb_order_details") public class OrderDetails {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Integer id;
 * 
 * @ManyToOne(fetch = FetchType.EAGER)
 * 
 * @JoinColumn(name = "product_id")//foreign key private Product product;
 * 
 * 
 * 
 * @Column(name = "quality", nullable = true) private Integer quantity;
 * 
 * @ManyToOne(fetch = FetchType.EAGER)
 * 
 * @JoinColumn(name = "order_id")//foreign key private Order order;
 * 
 * public OrderDetails(Integer id, Product product, Integer quantity, Order
 * order) { super(); this.id = id; this.product = product; this.quantity =
 * quantity; this.order = order; }
 * 
 * public OrderDetails() { super(); // TODO Auto-generated constructor stub }
 * 
 * public Integer getId() { return id; }
 * 
 * public void setId(Integer id) { this.id = id; }
 * 
 * public Product getProduct() { return product; }
 * 
 * public void setProduct(Product product) { this.product = product; }
 * 
 * public Integer getQuantity() { return quantity; }
 * 
 * public void setQuantity(Integer quantity) { this.quantity = quantity; }
 * 
 * public Order getOrder() { return order; }
 * 
 * public void setOrder(Order order) { this.order = order; }
 * 
 * 
 * }
 */