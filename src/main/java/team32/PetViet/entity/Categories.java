/*
 * package team32.PetViet.entity;
 * 
 * import java.util.Date; import java.util.HashSet; import java.util.Set;
 * 
 * import javax.persistence.CascadeType; import javax.persistence.Column; import
 * javax.persistence.Entity; import javax.persistence.FetchType; import
 * javax.persistence.GeneratedValue; import javax.persistence.GenerationType;
 * import javax.persistence.Id; import javax.persistence.OneToMany; import
 * javax.persistence.Table;
 * 
 * @Entity
 * 
 * @Table(name = "tbl_categories") public class Categories {
 * 
 * @Id
 * 
 * @GeneratedValue(strategy = GenerationType.IDENTITY)
 * 
 * @Column(name = "id") private Integer id;
 * 
 * @Column(name = "name", length = 200, nullable = false) private String name;
 * 
 * @Column(name = "description", length = 200, nullable = false) private String
 * description;
 * 
 * @Column(name = "created_at", nullable = true) private Date createdDate;
 * 
 * @Column(name = "updated_at", nullable = true) private Date updatedDate;
 * 
 * @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy =
 * "categories") private Set<Product> product = new HashSet<Product>();
 * 
 * }
 */