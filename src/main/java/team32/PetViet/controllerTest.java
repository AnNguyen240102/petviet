package team32.PetViet;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class controllerTest {
	@GetMapping("/index")
	public String index() {
		return "home";
	}
	
	@GetMapping("/contact")
	public String contact() {
		return "contact";
	}
	
	@GetMapping("/homeAdmin")
	public String homeAdmin() {
		return "admin/HomeAdmin";
	}

}
