package team32.PetViet.service;

import team32.PetViet.entity.Shiper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;  
import java.util.Optional;  


public interface  ShiperService {
	 List<Shiper> getAllShiper();  

	  void saveShiper(Shiper shiper);  

	  void deleteShiper(Integer id);  

	  Optional<Shiper> findShiperById(Integer id);  
	  
	  List<Shiper> allShiperOrderNow();  
}
