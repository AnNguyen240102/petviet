package team32.PetViet.service;

import team32.PetViet.entity.Shiper;  
import team32.PetViet.repository.ShiperRepository;  
import org.springframework.beans.factory.annotation.Autowired;  
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;  
import java.util.Optional;  

@Service 
public class ShiperServiceImpl implements ShiperService {
	@Autowired 
	private ShiperRepository shiperRepository;  
	
	@Override
	public List<Shiper> getAllShiper() {
		 return (List<Shiper>) shiperRepository.findAll();  
	}

	@Override
	public void saveShiper(Shiper shiper) {
		 shiperRepository.save(shiper); 
		
	}

	@Override
	public void deleteShiper(Integer id) {
		shiperRepository.deleteById(id); 
		
	}

	@Override
	public Optional<Shiper> findShiperById(Integer id) {
		return shiperRepository.findById(id);  
	}

	@Override
	public List<Shiper> allShiperOrderNow() {
		// TODO Auto-generated method stub
		return shiperRepository.searchShiperOrderNow();
	}



}
