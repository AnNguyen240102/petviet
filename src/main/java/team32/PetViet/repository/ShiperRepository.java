package team32.PetViet.repository;

import team32.PetViet.entity.Shiper;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;  
import org.springframework.stereotype.Repository;  

@Repository  
public interface ShiperRepository extends CrudRepository<Shiper, Integer> {
	@Query(value ="select s.name, Count(o.shipping_id) as soDonHang, date(now()) \r\n"
			+ "from tbl_shipers_transport s join tbl_order o\r\n"
			+ "on s.id = o.shipping_id\r\n"
			+ "where  Date(o.created_at) = date(now()) \r\n"
			+ "group by o.shipping_id", nativeQuery = true)
	List<Shiper> searchShiperOrderNow();
	
}